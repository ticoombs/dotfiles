set -g status-bg green
set -g status-keys vi
set -g status-right-length 60

# statusbar --------------------------------------------------------------

# Show messages and notifications for 2 seconds.
set -g display-time 3000

# Refresh the status bar every 1 second.
set -g status-interval 1

# The status bar itself.
set -g status-left "[#S]"
set -g status-interval 60
#set -g status-right "#(uptime)"

#### COLOUR

# default statusbar colors
set -g status-bg colour235 #base02
set -g status-fg colour136 #yellow

# default window title colors
set-window-option -g window-status-style fg=colour244
set-window-option -g window-status-style bg=default
set-window-option -g window-status-style dim

# active window title colors
set-window-option -g window-status-current-style fg=colour166 #orange
set-window-option -g window-status-current-style bg=default
set-window-option -g window-status-current-style bright

# pane border
set -g pane-border-style fg=colour235 #base02
set -g pane-active-border-style fg=colour240 #base01

# message text
set -g message-style bg=colour235 #base02
set -g message-style fg=colour166 #orange

# pane number display
set -g display-panes-active-colour colour33 #blue
set -g display-panes-colour colour166 #orange
