# Change the prefix key (screen friendly).
set -g prefix C-a
unbind-key C-b
bind-key C-a send-prefix

# Keys to switch session.
bind-key q switchc -t0
bind-key w switchc -t1
bind-key e switchc -t2

# Other key bindings.
bind-key i choose-window

bind-key m setw monitor-activity

bind-key y setw force-width 81
bind-key u setw force-width 0

bind-key D detach \; lock
bind-key N neww \; splitw -d

# Open new/split panes with the path of the current pane.
unbind c
bind c new-window -c '#{pane_current_path}'
unbind %
bind % split-window -h -c '#{pane_current_path}'
unbind '"'
bind '"' split-window -v -c '#{pane_current_path}'

# Vim-like key bindings for pane navigation (default uses cursor keys).
unbind h
bind h select-pane -L
unbind j
bind j select-pane -D
unbind k
bind k select-pane -U
unbind l # normally used for last-window
bind l select-pane -R

# Resizing (mouse also works).
unbind Left
bind -r Left resize-pane -L 5
unbind Right
bind -r Right resize-pane -R 5
unbind Down
bind -r Down resize-pane -D 5
unbind Up
bind -r Up resize-pane -U 5

# Fast toggle (normally prefix-l).
bind ^space last-window

# Intuitive window-splitting keys.
bind '|' split-window -h -c '#{pane_current_path}' # normally prefix-%
bind '\' split-window -h -c '#{pane_current_path}' # normally prefix-%
bind '-' split-window -v -c '#{pane_current_path}' # normally prefix-"

