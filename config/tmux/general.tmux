# Default global options.
set -g bell-action none
#set -g default-terminal "screen-256color"
set -g default-terminal tmux-256color
set -ga terminal-overrides ,*256col*:Tc
set -g history-limit 262144
# Start window and pane numbering at 1, (0 is too hard to reach).
set -g base-index 0
set -g pane-base-index 1

# Don't wait for an escape sequence after seeing C-a.
set -s escape-time 0

# Dynamically update iTerm tab and window titles.
set -g set-titles on

# But don't change tmux's own window titles.
set -w -g automatic-rename off

# Don't wrap searches; it's super confusing given tmux's reverse-ordering of
# position info in copy mode.
set -w -g wrap-search off

set -g aggressive-resize on # for attaching like screen
# Automatically renumber window numbers on closing a pane (tmux >= 1.7).
set -g renumber-windows on

# Mouse can be used to select panes, select windows (by clicking on the status
# bar), resize panes. For default bindings see `tmux list-keys` and `tmux
# list-keys -t vi-copy`.
set -g mouse on

# Default global window options
set-window-option -g mode-keys vi


# Restore pre-2.1 behavior of scrolling with the scroll wheel in Vim, less, copy
# mode etc, otherwise entering copy mode if not already in it.
bind-key -T root WheelUpPane \
  if-shell -Ft= '#{?pane_in_mode,1,#{mouse_any_flag}}' \
    'send -Mt=' \
    'if-shell -Ft= "#{alternate_on}" "send -t= Up" "copy-mode -et="'
bind-key -T root WheelDownPane \
  if-shell -Ft = '#{?pane_in_mode,1,#{mouse_any_flag}}' \
    'send -Mt=' \
    'if-shell -Ft= "#{alternate_on}"  "send -t= Down" "send -Mt="'

