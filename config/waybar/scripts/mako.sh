#!/bin/bash

COUNT=$(makoctl count waiting)
ENABLED=
DISABLED=
if [ $COUNT != 0  ]; then DISABLED=" $COUNT"; fi
if makoctl mode | grep -q "default" ; then echo $ENABLED; else echo $DISABLED; fi
