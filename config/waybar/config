{
    "layer": "top", // Waybar at top layer
    "position": "top", // Waybar position (top|bottom|left|right)
    "height": 25, // Waybar height (to be removed for auto height)
    "spacing": 2, // Gaps between modules (4px)
    "modules-left": [
		"sway/workspaces", 
        "sway/mode", 
        //"custom/media", 
        //"mpd", 
        "custom/scratchpad-indicator"],
    "modules-center": ["sway/window"],
    "modules-right": [ 
        "custom/recording",
        "custom/launcher", 
        "idle_inhibitor", 
        "custom/mako", 
        //"pulseaudio", 
        "wireplumber", 
        "custom/network",
        "network", 
        //"cpu", 
        //"memory", 
        //"temperature",  
        //"temperature#GPU", 
        //"custom/blog-stats", 
        "clock", 
        "tray",
        "custom/power"
        ],

    // Modules configuration
    "sway/workspaces": {
         "format": "{name}: {icon}",
         "format-icons": {
             "1": "🦊",
             "4": "\uf1b6", //STEAM
             "5": "\uf11b",
             "9": "📧",
             "urgent": "",
             "default": ""
         },
         //"persistent_workspaces": {
         //    "1": "[DP-1]",
         //    "2": "[HDMI-A-1]"
         //}
     },
    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": "{name} {icon}",
        "format-icons": {
            "locked": "",
            "unlocked": ""
        }
    },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "sway/window": {
        "max-length": 70
    },
    "mpd": {
        "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) {songPosition}|{queueLength} {volume}% ",
        "format-disconnected": "Disconnected ",
        "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
        "unknown-tag": "N/A",
        "interval": 2,
        "consume-icons": {
            "on": " "
        },
        "random-icons": {
            "off": "<span color=\"#f53c3c\"></span> ",
            "on": " "
        },
        "repeat-icons": {
            "on": " "
        },
        "single-icons": {
            "on": "1 "
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        "tooltip-format": "MPD (connected)",
        "tooltip-format-disconnected": "MPD (disconnected)",
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format-alt": " {:%Y-%m-%d}"
    },
    "cpu": {
        "format": "{usage}% 🖥",
        "on-click": "exec alacritty --class custom-pop-up -e glances",
        "tooltip": false
    },
    "memory": {
        "on-click": "exec alacritty --class custom-pop-up -e htop",
        "format": "{}% "
    },
    "temperature": {
        "thermal-zone": 1,
        "hwmon-path": "/sys/class/hwmon/hwmon1/temp1_input",
        "critical-threshold": 40,
        "format-critical": "{temperatureC}°C {icon}",
        "format": "",
        "format-icons": ["", "", ""]
    },
    "temperature#GPU": {
        // "thermal-zone": 2,
        "hwmon-path": "/sys/class/drm/card0/device/hwmon/hwmon2/temp1_input",
        "critical-threshold": 60,
        "format-critical": "{temperatureC}°C {icon}",
        "format": "",
        "format-icons": ["", "", ""]
    },
    "backlight": {
        // "device": "acpi_video1",
        "on-scroll-up": "light -T 1.1",
        "on-scroll-down": "light -T 0.9",
        "format": "{percent}% {icon}",
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{capacity}% {icon}",
        "format-charging": "{capacity}% ",
        "format-plugged": "{capacity}% ",
        "format-alt": "{time} {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""]
    },
    "network": {
        "family": "ipv4",
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname} 🖧",
        "format-linked": "{ifname} (No IP) 🖧",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ipaddr}/{cidr} 🖧"
    },
    "network#net6": {
        "family": "ipv6",
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname} 🖧",
        "format-linked": "{ifname} (No IP) 🖧",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ifname}: {ipaddr}/{cidr}"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} {format_source}",
        "format-bluetooth-muted": " {icon} {format_source}",
        "format-muted": " {format_source}",
        "format-source": "{volume}% 🕪",
        "format-source-muted": "🕨",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click-right": "pavucontrol",
        "on-click": "$HOME/bin/soundmenu"
    },
    "wireplumber": {
        "scroll-step": 1, // %, can be a float
        "format": "{volume}% {node_name}",
        "max-length": 16,
        //"format-bluetooth": "{volume}% {icon} {node_name}",
        //"format-bluetooth-muted": " {icon} {node_name}",
        //"format-muted": "  {node_name}",
        //"format-icons": {
        //    "hands-free": "",
        //    "headset": "",
        //    "phone": "",
        //    "portable": "",
        //    "car": "",
        //    "default": ["", "", ""]
        //},
        "on-click-right": "pavucontrol",
        "on-click": "$HOME/bin/soundmenu"
    },
    "custom/media": {
        "format": "{icon} {}",
        "return-type": "json",
        "max-length": 40,
        "format-icons": {
            "spotify": "",
            "default": ""
        },
        "escape": true,
        "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
        // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
    },
	"custom/scratchpad-indicator": {
		"interval": 3,
		"return-type": "json",
		"exec": "swaymsg -t get_tree | jq --unbuffered --compact-output '( select(.name == \"root\") | .nodes[] | select(.name == \"__i3\") | .nodes[] | select(.name == \"__i3_scratch\") | .focus) as $scratch_ids | [..  | (.nodes? + .floating_nodes?) // empty | .[] | select(.id |IN($scratch_ids[]))] as $scratch_nodes | { text: \"\\($scratch_nodes | length)\", tooltip: $scratch_nodes | map(\"\\(.app_id // .window_properties.class) (\\(.id)): \\(.name)\") | join(\"\\n\") }'",
		"format": "{} 🗗",
		"on-click": "exec swaymsg 'scratchpad show'",
		"on-click-right": "exec swaymsg 'move scratchpad'"
	},
	"custom/dunst": {
		"exec": "~/.config/waybar/scripts/dunst.sh",
		"on-click": "dunstctl set-paused toggle",
		"format": " {} ",
		"restart-interval": 1
	},
    "custom/recording": {
        "exec": "exec ~/bin/record-screend",
        "exec-on-event": false,
        "on-click": "pkill -INT -P \"$(pgrep -xo record-screen)\" wf-recorder"
    },
	"custom/mako": {
		"exec": "~/.config/waybar/scripts/mako.sh",
		"on-click": "makoctl set-mode do-not-disturb",
        "on-click-right": "makoctl set-mode default",
		"format": " {} ",
		"restart-interval": 1
	},
    "custom/gpu-usage": {
    	"exec": "cat /sys/class/hwmon/hwmon2/device/gpu_busy_percent",
    	"format": "GPU: {}%",
		"return-type": "",
		"interval": 1
	},
	"custom/launcher": {
    	"format":"{icon}",
        "format-icons": {
            "default": "\uf17c", //linux
        },
        "on-click": "exec wofi --show drun",
    	"tooltip": false,
	},
	"custom/power": {
    	"format":" ⏻  ",
    	"on-click": "exec wlogout",
    	"tooltip": false,
   	},
    "custom/network": {
        "exec": "~/.config/waybar/scripts/network_traffic.sh -t2 enp4s0f0",
        "return-type": "json",
        //"format": "Speed: {}", //optional
    }

}

