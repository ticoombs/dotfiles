#!/usr/bin/env bash
# Original based off: https://github.com/nicknisi/dotfiles/blob/master/install.sh

echo " - Installing dotfiles"

source install/link.sh

if [ -f "/etc/gentoo-release" ]; then
    echo " - Running on Gentoo"
    source install/portage-defaults.sh
elif [ -f "/usr/lib/os-release" ]; then
    echo " - Running on Debian derivitive"
    source install/apt-defaults.sh
fi

echo " - Configuring zsh as default shell"
chsh -s $(which zsh)

source install/common.sh

echo "Done."
