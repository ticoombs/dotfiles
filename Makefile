

install: install_bin install_config 

install_bin: bin/*
	for app in $^; do \
		install -D $${app} ${HOME}/bin/ ; \
	done

install_nautilus: bin/up
	[[ -d ${HOME}/.local/share/nautilus/scripts/ ]] || exit 0
	for app in $^; do \
		install -D $${app} ${HOME}/.local/share/nautilus/scripts/ ; \
	done

install_config: config/*
	for config in $^; do \
		ln -sf $${config} ${HOME}/.config/} ; \
	done
