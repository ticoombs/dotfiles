#!/bin/bash

# This script takes a screenshot and pixellates it, then uses it as the i3lock
# background.  It was borrowed from here:
# https://faq.i3wm.org/question/83/how-to-run-i3lock-after-computer-inactivity.1.html

set -e

IMG_PATH=/tmp/i3-pixlock.png
LOCK_ICO=$HOME/.i3/i3lock.png
scrot $IMG_PATH

# Pixellate it 20x
convert $IMG_PATH -scale 5% -scale 2000% $IMG_PATH

if [[ -f $LOCK_ICO ]]; then
    # placement x/y
    PX=0
    PY=0
    # lockscreen image info
    R=$(file $LOCK_ICO | grep -o '[0-9]* x [0-9]*')
    RX=$(echo $R | cut -d' ' -f 1)
    RY=$(echo $R | cut -d' ' -f 3)
 
    SR=$(xrandr --query | grep ' connected' | sed 's/primary //' | cut -f3 -d' ')
    for RES in $SR
    do
        # monitor position/offset
        SRX=$(echo $RES | cut -d'x' -f 1)                   # x pos
        SRY=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 1)  # y pos
        SROX=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 2) # x offset
        SROY=$(echo $RES | cut -d'x' -f 2 | cut -d'+' -f 3) # y offset
        PX=$(($SROX + $SRX/2 - $RX/2))
        PY=$(($SROY + $SRY/2 - $RY/2))
 
        convert $IMG_PATH $LOCK_ICO -geometry +$PX+$PY -composite -matte  $IMG_PATH
        echo "done"
    done
fi

# Lock screen displaying this image.
i3lock -i $IMG_PATH

# Now turn off the screen after 60 seconds, as long as i3lock is still running
#xset dpms force off
