# Actual Usefull commands
alias lefty='xmodmap -e "pointer = 3 2 1"'
alias righty='xmodmap -e "pointer = 1 2 3"'
alias hdmitoggle='togglehdmi'
alias togglehdmi="swaymsg output HDMI-A-1 disable; swaymsg output HDMI-A-1 enable;"
alias dptoggle='swaymsg output DP-3 disable; swaymsg output DP-1 enable'
alias sound='pavucontrol'
alias LS='ls'
alias CD='cd'
alias FG='fg'
alias HTOP='htop'
alias emerge-world-estimate='emerge -puND @world | genlop --pretend'
alias emerge-system-estimate='emerge -puND @system | genlop --pretend'
alias :wq='echo "This isnt VIM! exiting anyway"; sleep 1; exit'
alias awksum="awk '{sum += \$1} END {print sum}'"
# Hack tmux to use 256 colors
alias tmux="TERM=xterm-256color tmux"
alias tmxu='tmux'
alias atmux="tmux a || tmux"
alias pip-env="source $HOME/.local/pip-env/bin/activate"

# AllHailMyMightySword
export EDITOR=vim
# Set Python3 as default
alias python="python3"
# Todo.sh
alias td="todo.sh"
# Weather
alias weather="curl -4 https://wttr.in/Melbourne"

# File System Stuff
alias l="ls -lah ${colorflag}"
alias la="ls -AF ${colorflag}"
alias ll="ls -lFh ${colorflag}"
alias lld="ls -l | grep ^d"
alias rmf="rm -rf"

# IP addresses
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias myip6="dig +short -t aaaa myip.opendns.com @resolver1.opendns.com"

# Helpers
alias grep='grep --color=auto'
alias df='df -h' 
alias du='du -h -c'
alias digs='dig +short'
alias more='less'
alias gcm="git checkout main"
alias gco="git checkout"
alias gcmsg="git commit -m"

# Custom Git
alias gbclean='echo "Removing Merged Branches"; sleep 2; git checkout master && git branch --merged | egrep -v "(^\*|master|dev)" | while read b; do git branch -d $b; done'
alias grclean='echo "Remote Pruning with against Origin"; sleep 2; git checkout master && git remote prune origin | grep origin\/ | sed "s/^origin\///g" | while read b; do git branch -D $b; done'
alias gpr="git pull-request"

# Blogging
alias blogging='bundle install && bundle exec jekyll serve --watch'
alias blog-deploy='bundle exec jekyll clean; bundle exec jekyll build; rsync -aP _site/ blog.slowb.ro:/opt/nginx-blog/web/ --delete-during'

# Fun
alias huh='echo "¯\(°_o)/¯"'
alias shrug='echo "¯\_(ツ)_/¯"'
alias eac3to="wine ~/repos/eac3to/eac3to.exe 2>/dev/null"
alias eacout='wine ~/repos/eac3to/eac3to.exe 2>/dev/null | tr -cd "\11\12\15\40-\176"'

alias window-info="echo 'Focus window now!';sleep 2; swaymsg -t get_tree | jq -r '..|try select(.focused == true)'"
alias window-kill="window-info | grep pid | grep -oE '[0-9]+' | xargs kill -9"
