# Actual Usefull commands
alias lefty='xmodmap -e "pointer = 3 2 1"'
alias righty='xmodmap -e "pointer = 1 2 3"'
alias leftscreen='xrandr --output DVI-I-2 --auto --left-of DVI-I-1'
alias sound='pavucontrol'
alias LS='ls'
alias CD='cd'
alias emerge-world-estimate='emerge -puND @world | genlop --pretend'
alias emerge-system-estimate='emerge -puND @system | genlop --pretend'
# Hack tmux to use 256 colors
alias tmux="TERM=xterm-256color tmux"
alias :wq='echo "This isnt VIM! exiting anyway"; exit'
alias tmxu='tmux'
alias awksum="awk '{sum += \$1} END {print sum}'"

# AllHailMyMightySword
export EDITOR=vim
# Set Python3 as default
alias python="python3"
# Todo.sh
alias td="todo.sh"
# Weather
alias weather="curl -4 https://wttr.in/Melbourne"

# File System Stuff
alias l="ls -lah ${colorflag}"
alias la="ls -AF ${colorflag}"
alias ll="ls -lFh ${colorflag}"
alias lld="ls -l | grep ^d"
alias rmf="rm -rf"

# IP addresses
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"

# Helpers
alias grep='grep --color=auto'
alias df='df -h' 
alias du='du -h -c'
alias digs='dig +short'
alias more='less'

# Custom Git
alias gbclean='echo "Removing Merged Branches"; sleep 2; git checkout master && git branch --merged | egrep -v "(^\*|master|dev)" | while read b; do git branch -d $b; done'
alias grclean='echo "Remote Pruning with against Origin"; sleep 2; git checkout master && git remote prune origin | grep origin\/ | sed "s/^origin\///g" | while read b; do git branch -D $b; done'
alias gpr="git pull-request"

# Blogging
alias blogging='bundle install && bundle exec jekyll serve --watch'
alias blog-deploy='bundle exec jekyll clean; bundle exec jekyll build; rsync -aP _site/ blog.slowb.ro:/opt/nginx-blog/web/ --delete-during'

# Fun
alias huh='echo "¯\(°_o)/¯"'
alias shrug='echo "¯\_(ツ)_/¯"'
