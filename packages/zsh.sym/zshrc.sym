export ZSH="$HOME/.oh-my-zsh"
GOPATH=$HOME/gocode

# Look in ~/.oh-my-zsh/themes/
ZSH_THEME="kafeitu-dirtrim"

SAVEHIST=100000

# Aliases
[ -f $HOME/.zsh/aliases.zsh ] && . $HOME/.zsh/aliases.zsh
[ -f $HOME/.zsh/work-aliases.zsh ] && . $HOME/.zsh/work-aliases.zsh

# Gentoo Environments
[ -f /etc/profile.env ] && . /etc/profile.env

umask 022
# Uncomment following line if you want to disable autosetting terminal title. (Useful for screen!)
#DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(git github sprunge apt bower cap common-aliases urltools wd Pass sudo python pip npm git-extras encode64 zsh-completions)

setopt extendedglob
setopt autocd
setopt hist_ignore_all_dups
setopt share_history
#setopt correctall

#Bindkeys for term's
bindkey '[1~' beginning-of-line
bindkey '[4~' end-of-line

# First source the latest Gentoo ZSH Profile script
[ -f /etc/zsh/zprofile ] && . /etc/zsh/zprofile

# Because for now we are a zsh fanboy
. $ZSH/oh-my-zsh.sh

# Customize PATH (Making sure we get env path upto-date)
PATH=$PATH
# RVM
[ -d /usr/local/rvm/bin ] && PATH=$PATH:/usr/local/rvm/bin
# Games
[ -d /usr/games/bin ] && PATH=$PATH:/usr/games:/usr/games/bin
# Local Bin
[ -d $HOME/bin ] && PATH=$PATH:~/bin
[ -d $HOME/.local/bin ] && PATH=$PATH:~/.local/bin
# Export for the Environment
export PATH=$PATH
export GIT_EDITOR=vim

# Add Time to the RIGHT side of our prompt
RPROMPT="[%*]"

# GPG or PGP we are not picky, but we are users
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
gpg-connect-agent /bye
export GPG_TTY=$(tty)

# Setup and load our auto-completion
zstyle ':completion::complete:*' use-cache 1
autoload -U compinit promptinit
compinit

[ -d $HOME/.nvm ] && export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh"  ] && . "$NVM_DIR/nvm.sh"
[ -s /usr/local/bin/aws_zsh_completer.sh ] && . "/usr/local/bin/aws_zsh_completer.sh"

# fzf auto adds this line even though $HOME is way better than ~
[ -f ~/.fzf.zsh ] && . ~/.fzf.zsh
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
