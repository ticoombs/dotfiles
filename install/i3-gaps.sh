#!/usr/env/bin bash

#Our Repo Directory
REPODIR=$1

git clone https://github.com/Airblader/i3.git $REPODIR/i3-gaps && \
cd $REPODIR/i3-gaps && \
git checkout gaps && \
git pull &&\
make && \
sudo make install
