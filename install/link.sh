#!/usr/bin/env bash

DOTFILES=$HOME/Documents/repos/dotfiles
DEBUG=$3

function link {
    file=$1
    target=$2
    echo " - Creating symlink for $file to $target"
    if ! [ $DEBUG ]; then 
        ln -sf $file $target
    fi
}

echo " - Creating Symlinks"

linkables=$( find -H "$DOTFILES" -maxdepth 3 -name '*.sym' )
for file in $linkables ; do
    target="$HOME/.$( basename $file '.sym' )"
    if [ -f $target ]; then
        diff $file $target > /dev/null 2>&1
        if [ $? = 1 ]; then
            # Diff returns 1 for existance but different files
            link $file $target
        else
            echo " + ${target#$HOME} already installed... Skipping."
        fi
    elif [ -d $target ]; then
        echo " * $target is a directory NOT a link. This shouldnt happen"
    else
        link $file $target
    fi
done

echo -e " - Installing to ~/.config"
if [ ! -d $HOME/.config ]; then
    echo "Creating ~/.config"
    mkdir -p $HOME/.config
fi

for config in $DOTFILES/config/*; do
    target=$HOME/.config/$( basename $config )
    if [ -e $target ]; then
        echo " + ${target#$HOME} already exists... Skipping."
    else
        link $config $target
    fi
done
