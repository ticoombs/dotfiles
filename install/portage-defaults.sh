#!/usr/env/bin bash

sudo emerge --sync
# All the things we need
sudo emerge -v git \
    flake8 \
    stow \
    zsh \
    layman \
    firefox \
    thunderbird \
    irssi \
    tmux \
    screen \
    pip \
    ntfs-3g \
    pulseaudio \
    inconsolata \
    lm_sensors \
    sysstat \
    ffmpegthumbnailer

# Install after i3-gaps installed
sudo emerge -v dmenu \
    dunst \
    i3lock \
    i3status \
    compton \
    redshift \
    scrot \
    x11-misc/rofi \
    rxvt-unicode \
    w3m \
    playerctl \
    amixer 

# Allow a pop-up for authentication via sudo for gdk based apps
sudo emerge -v net-misc/ssh-askpass-fullscreen

# This takes a while... better ask
sudo emerge -va libreoffice-bin \
    gimp \
    wine-staging 


