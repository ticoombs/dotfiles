GIT=$HOME/Documents/repos

# Setup Vim
vim +PluginInstall +qall
# 
cp -R $HOME/Documents/repos/dotfiles/bin $HOME/

cd $GIT
# Install GOTO - Navigate long command lines using a minimalistic char-based decision tree.
git clone https://github.com/Fakerr/goto.git

cd $GIT
# Install thefuck - https://github.com/nvbn/thefuck
git clone https://github.com/nvbn/thefuck && cd thefuck && pip install --user -r requirements.txt && python setup.py install 

cd $GIT
# z - jump around based on "frecency"
git clone https://github.com/rupa/z && cd z && make && sudo make install

cd $GIT
# fz - fuzzy tab completion of Z
git clone https://github.com/changyuheng/fz 

# fzf - CLI Fuzzy history finder is handled by VIM plugin
